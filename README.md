[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687329.svg)](https://doi.org/10.5281/zenodo.4687329)

# 'Restricted solar thermal energy open field energy potential for the EU28 + Switzerland, Norway and Iceland for the year 2012'


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation
This dataset shows the estimated restricted energy potential using open-field solar thermal collector fields in EU28 (+ CHH/NO/IS) on hectare (ha) level. 

The dataset builds on the average annual solar radiationon the optimal inclined plane [1] - which is also part of the Hotmaps open source data set. Restrictions of the potential are considered as follows.
Based on Corine Land Cover data [2], all land usage data except the folling list are excluded.

For the potential, only the following land-use types are considered:
<table>
  <tr>
    <td>GRID_CODE</td>
    <td>CLC_CODE</td>
    <td>LABEL1-3</td>

  </tr>
  <tr>
    <td>12</td>
    <td>211</td>
    <td>Agricultural areas => Arable land => Non-irrigated arable land</td>
  </tr>
  <tr>
    <td>18</td>
    <td>231</td>
    <td>Agricultural areas => Pastures => Pastures</td>
  </tr>
  <tr>
    <td>19</td>
    <td>241</td>
    <td>Agricultural areas => Heterogeneous agricultural areas => Annual crops associated with permanent crops</td>
  </tr>
  <tr>
    <td>20</td>
    <td>242</td>
    <td>Agricultural areas => Heterogeneous agricultural areas => Complex cultivation patterns</td>
  </tr>
  <tr>
    <td>21</td>
    <td>243</td>
    <td>Agricultural areas => Heterogeneous agricultural areas => Land principally occupied by agriculture, with significant areas of natural vegetation</td>
  </tr>
  <tr>
    <td>22</td>
    <td>244</td>
    <td>Agricultural areas => Heterogeneous agricultural areas => Agro-forestry areas	</td>
  </tr>
  <tr>
    <td>29</td>
    <td>324</td>
    <td>Forest and semi natural areas => Scrub and/or herbaceous vegetation associations => Transitional woodland-shrub	</td>
  </tr>
    <tr>
    <td>32</td>
    <td>333</td>
    <td>Forest and semi natural areas => Open spaces with little or no vegetation => Sparsely vegetated areas</td>
  </tr>
</table>


Furthermore, it is assumed that:
- the efficiency of solar thermal collector is: 0.38 + min(0.03, max(-0.05, 0.05 * (3000-HDD)/2000)),
- the solar collector area is limited to 0.25 m2 per 1 m2 plot area,
- the available plot area is reduced by the 5 x total building footprint (background data set of the Hotmaps project, unpublished, see also [3]) in each cell.


### Limitations of the dataset
For the use and the estimation of the reliability of the data it is important to keep in mind that the data maps build on a statistical approach and do not take site specific or local conditions into account.

For the residential heated gross floor area, statistical data are available for most countries on the level of NUTS3. Again, manually performed data quality checks indicate that results are plausible on the hectare level of most regions. However, as of now, we do not factor in the fact, that the heat area per inhabitant often decrease with an increasing population density. For  NUTS3  regions  with  a  strong  urban  versus  rural  area  gradient,  this  might  lead  to  overestimation of the heated residential gross floor area in urban areas. Regarding the heated gross floor area of non‐residential buildings, data sources are even uncertain on the NUTS0 level. Data quality checks indicate that the sum of residential and non‐ residential heated gross floor area are plausible as well as the ratio between residential and non‐residential gross floor area,  even  though  the  later  indicator  might  not  hold  for  grid  cells which contain only  few  buildings.


### References
[1] Solar radiation Raster @ 100x100m annual mean, https://gitlab.com/hotmaps/climate/climate_solar_radiation - (data update from: 30/01/2019, commit: 62e979de)

[2] Corine Land Cover (CLC) 2012, Version 18.5.1.](https://land.copernicus.eu/pan-european/corine-land-cover/clc-2012/view) 

[35] Restricted solar thermal roof top potetnial (https://gitlab.com/hotmaps/potential/potential_solarthermal_collectors_rooftop)


## How to cite
Andreas Mueller, "Restricted solar thermal energy open field energy potential for the EU28 + Switzerland, Norway and Iceland for the year 2012", Technische Universität Wien, 2019. https://gitlab.com/hotmaps/potential/potential_solarthermal_collectors_open_field

## Authors
Andreas Mueller <sup>*</sup>

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/), Institute of Energy Systems and Electrical Drives, Gusshausstrasse 27-29/370, 1040 Wien


## License
Copyright © 2016-2019: Andreas Mueller
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

